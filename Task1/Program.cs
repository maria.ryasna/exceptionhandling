﻿using System;

namespace Task1
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Enter some text:");
            string stringValue = Console.ReadLine();

            try
            {
                PrintBySymbol(stringValue);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine($"[ERROR]: {ex.Message}");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("[ERROR]: Value of {0} could not be empty", ex.Message);
            }

            Console.ReadKey();
        }

        private static void PrintBySymbol(string stringValue)
        {
            if (stringValue == null)
            {
                throw new ArgumentNullException(nameof(stringValue));
            }
            if (stringValue == string.Empty)
            {
                throw new ArgumentException(nameof(stringValue));
            }
            Console.WriteLine($"First character is: '{stringValue[0]}'");
        }
    }
}