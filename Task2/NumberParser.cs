﻿using System;

namespace Task2
{
    public class NumberParser : INumberParser
    {
        public int Parse(string stringValue)
        {
            if (stringValue is null)
            {
                throw new ArgumentNullException(nameof(stringValue));
            }

            string stringWithoutWhiteSpaces = stringValue.Trim(' ');

            if (stringWithoutWhiteSpaces == string.Empty)
            {
                throw new FormatException(nameof(stringValue));
            }

            string stringParse = GetStringWithoutSign(stringWithoutWhiteSpaces);

            if (!IsNumberCorrectFormat(stringParse))
            {
                throw new FormatException(nameof(stringParse));
            }

            int[] array = GetNumberArray(stringParse.TrimStart('0'));
            int sign = GetDigitSign(stringWithoutWhiteSpaces[0]);

            if (!IsNumberInRangeInt32(array, sign))
            {
                throw new OverflowException(nameof(stringParse));
            }
            
            int result = 0;

            foreach (int number in array)
            {
                result = result * 10 + number;
            }
            
            return result * sign;
        }

        private bool IsNumberCorrectFormat(string stringValue)
        {
            foreach (char symbol in stringValue)
            {
                if (symbol < 48 || symbol > 57)
                {
                    return false;
                }
            }
            return true;
        }

        private int GetDigitSign(char symbol)
        {
            if (symbol == '-')
            {
                return -1;
            }
            return 1;
        }

        private string GetStringWithoutSign(string stringValue)
        {
            if (stringValue[0] == '-' || stringValue[0] == '+')
            {
                return stringValue.Substring(1);
            }
            return stringValue;
        }

        private int[] GetNumberArray(string stringValue)
        {
            int[] array = new int[stringValue.Length];
            for (int i = 0; i < stringValue.Length; i++)
            {
                array[i] = stringValue[i] - '0';
            }
            return array;
        }

        private bool IsNumberInRangeInt32(int[] array, int sign)
        {
            if (array.Length < 10)
            {
                return true;
            }
            if (array.Length > 10)
            {
                return false;
            }
            int[] arrayToCompare = GetInt32MaxValueArray(sign);
            for (int i = 0; i < 10; i++)
            {
                if (array[i] > arrayToCompare[i])
                {
                    return false;
                }
            }
            return true;
        }

        private int[] GetInt32MaxValueArray(int sign)
        {
            int max = Int32.MaxValue;
            int result = max;
            int[] array = new int[10];
            for (int i = 9; i != -1; i--)
            {
                array[i] = result % 10;
                result = result / 10;
            }

            if (sign == -1)
            { 
                array[9] = array[9] + 1;
            }
            return array;
        }
    }
}